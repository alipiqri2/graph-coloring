# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './screens/ui_files/benchmark.ui'
#
# Created by: PyQt6 UI code generator 5.15.4
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from functools import partial

from PyQt6 import QtCore, QtGui, QtWidgets

from screens.backtrackingScreen import Ui_backtrackingScreen
from screens.bbScreen import Ui_bbScreen


class Ui_benchmarkScreen(object):
    def setupUi(self, benchmarkScreen, pile):
        benchmarkScreen.setObjectName("benchmarkScreen")
        benchmarkScreen.resize(800, 600)
        self.centralwidget = QtWidgets.QWidget(benchmarkScreen)
        self.centralwidget.setObjectName("centralwidget")
        self.bbButton = QtWidgets.QPushButton(self.centralwidget)
        self.bbButton.setGeometry(QtCore.QRect(260, 130, 241, 41))
        font = QtGui.QFont()
        font.setFamily("Verdana")
        font.setPointSize(14)
        self.bbButton.setFont(font)
        self.bbButton.setStyleSheet(
            "background-color: rgb(24, 53, 76);\n" "color: rgb(230, 236, 235);"
        )
        self.bbButton.setObjectName("bbButton")
        self.backtrackingButton = QtWidgets.QPushButton(self.centralwidget)
        self.backtrackingButton.setGeometry(QtCore.QRect(260, 190, 241, 41))
        font = QtGui.QFont()
        font.setFamily("Verdana")
        font.setPointSize(14)
        self.backtrackingButton.setFont(font)
        self.backtrackingButton.setStyleSheet(
            "background-color: rgb(24, 53, 76);\n" "color: rgb(230, 236, 235);"
        )
        self.backtrackingButton.setObjectName("backtrackingButton")

        self.screenTitle = QtWidgets.QLabel(self.centralwidget)
        self.screenTitle.setGeometry(QtCore.QRect(120, 30, 541, 71))
        font = QtGui.QFont()
        font.setFamily("Verdana")
        font.setPointSize(26)
        font.setBold(True)
        font.setWeight(75)
        self.screenTitle.setFont(font)
        self.screenTitle.setLayoutDirection(QtCore.Qt.LayoutDirection.LeftToRight)
        self.screenTitle.setStyleSheet(
            "background-color: rgb(255, 255, 255);\n" "color: rgb(24, 53, 75);\n" ""
        )
        self.screenTitle.setObjectName("screenTitle")

        benchmarkScreen.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(benchmarkScreen)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 26))
        self.menubar.setObjectName("menubar")
        benchmarkScreen.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(benchmarkScreen)
        self.statusbar.setObjectName("statusbar")
        benchmarkScreen.setStatusBar(self.statusbar)

        self.retranslateUi(benchmarkScreen)
        QtCore.QMetaObject.connectSlotsByName(benchmarkScreen)
        self.bbButton.clicked.connect(partial(self.gotoBb, benchmarkScreen, pile))
        self.backtrackingButton.clicked.connect(partial(self.gotoBacktracking, benchmarkScreen, pile))

    def retranslateUi(self, benchmarkScreen):
        _translate = QtCore.QCoreApplication.translate
        benchmarkScreen.setWindowTitle(_translate("benchmarkScreen", "MainWindow"))
        self.bbButton.setText(_translate("benchmarkScreen", "Branch and Bound"))
        self.backtrackingButton.setText(_translate("benchmarkScreen", "Backtracking"))
        self.screenTitle.setText(_translate("benchmarkScreen", "Choose the algorithm"))

    def gotoBb(self, screen, pile):
        print("Go to B&b")
        # pushScreen(self)
        pile.append(self)
        print(pile)
        ui = Ui_bbScreen()
        ui.setupUi(screen, pile)

    def gotoBacktracking(self, screen, pile):
        print("Go to Backtracking")
        # pushScreen(self)
        pile.append(self)
        print(pile)
        ui = Ui_backtrackingScreen()
        ui.setupUi(screen, pile)


if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    benchmarkScreen = QtWidgets.QMainWindow()
    ui = Ui_benchmarkScreen()
    ui.setupUi(benchmarkScreen)
    benchmarkScreen.show()
    sys.exit(app.exec_())

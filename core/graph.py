from math import inf

import matplotlib.pyplot as plt
import networkx as nx


class graph:

    def __init__(self):
        self.adj_matrix = None
        self.adj_list = None
        self.edges = None
        self.num_vertices = 0
        self.num_edges = 0
        self.colors = None
        self.degrees = None
        self.optimum = (
            inf
        )

    def read(self, input_file, mode, indexation):
        with open(input_file, "r") as f:
            input = f.read().strip().split("\n")

            self.num_vertices = int(input[0].split()[0])
            self.num_edges = int(input[0].split()[1])

            self.adj_matrix = [
                [0 for _ in range(self.num_vertices)] for _ in range(self.num_vertices)
            ]
            self.adj_list = [[] for _ in range(self.num_vertices)]
            self.edges = []

            self.colors = [-1 for _ in range(self.num_vertices)]

            self.degrees = [-1 for _ in range(self.num_vertices)]

            if mode == "--adjlist":
                adj_list = input[1:]

                for lst in adj_list:
                    vertex, neighbors = (
                        int(lst.split()[0]),
                        [int(x) for x in lst.split()[1:]],
                    )

                    if indexation == "--1-indexed":
                        vertex -= 1

                    assert 0 <= vertex and vertex < self.num_vertices

                    self.adj_list[vertex] = neighbors

                    self.degrees[vertex] = len(neighbors)

                    for neighbor in neighbors:
                        self.adj_matrix[vertex][neighbor] = 1
                        if (neighbor, vertex) not in self.edges:
                            self.edges.append((vertex, neighbor))
            else:
                edges = input[1:]

                assert len(edges) == self.num_edges

                for edge in edges:
                    edge = [int(x) for x in edge.split()]
                    assert len(edge) == 2

                    if indexation == "--1-indexed":
                        edge[0] -= 1
                        edge[1] -= 1

                    assert 0 <= edge[0] and edge[0] < self.num_vertices
                    assert 0 <= edge[1] and edge[1] < self.num_vertices

                    self.adj_list[edge[0]].append(edge[1])
                    self.adj_list[edge[1]].append(edge[0])

                    self.adj_matrix[edge[0]][edge[1]] = 1
                    self.adj_matrix[edge[1]][edge[0]] = 1

                    self.edges.append(edge)

                    self.degrees[edge[0]] += 1
                    self.degrees[edge[1]] += 1

    def read(self, input_file):
        try:
            f = open(input_file, "r")

            line = f.readline()

            while line:
                if line[0] == "p":
                    splited_line = line.split(" ")
                    self.num_vertices = int(splited_line[2])
                    self.optimum = self.num_vertices + 1
                    self.num_edges = int(splited_line[3])
                    break
                line = f.readline()

            self.adj_matrix = [
                [0 for _ in range(self.num_vertices)] for _ in range(self.num_vertices)
            ]
            self.adj_list = [[] for _ in range(self.num_vertices)]
            self.edges = []

            self.colors = [-1 for _ in range(self.num_vertices)]

            self.degrees = [-1 for _ in range(self.num_vertices)]

            while line:
                if line[0] == "e":
                    line = line.replace("e ", "").split()
                    edge = [int(e) - 1 for e in line]

                    if not all(e < self.num_vertices for e in edge):
                        raise ValueError(
                            f"vertex number > number of vertices , Edge {edge[0]} {edge[1]}"
                        )
                        exit()
                    if len(edge) != 2:
                        raise ValueError(f"Invalid number of vertices {len(edge)}")
                        exit()

                    self.adj_list[edge[0]].append(edge[1])
                    self.adj_list[edge[1]].append(edge[0])

                    self.adj_matrix[edge[0]][edge[1]] = 1
                    self.adj_matrix[edge[1]][edge[0]] = 1

                    if (edge[1], edge[0]) not in self.edges:
                        self.edges.append((edge[0], edge[1]))

                    self.degrees[edge[0]] += 1
                    self.degrees[edge[1]] += 1
                line = f.readline()
            f.close()
        except FileNotFoundError:
            print("Wrong file or file path")
            exit()

    def re_initialize_graph(self):
        self.colors = [-1 for _ in range(self.num_vertices)]
        self.optimum = self.num_vertices

    def get_neighbors(self, vertex):
        assert vertex < self.num_vertices
        return self.adj_list[vertex]

    def get_neighbors_colors(self, vertex, sub_coloring):
        neighbors = self.get_neighbors(vertex)
        neighbors_colors = [sub_coloring[i] for i in neighbors if sub_coloring[i] != -1]
        return neighbors_colors

    def update_solution(self, new_solution):
        num_of_colors = len(set(new_solution))
        if num_of_colors < self.optimum:
            self.optimum = num_of_colors
            self.colors = new_solution
            return True
        return False

    def validate_solution(self):
        for vertex in range(self.num_vertices):
            if self.colors[vertex] == -1:
                return False
            if self.colors[vertex] in self.get_neighbors_colors(vertex, self.colors):
                return False
        return True

    def validate_candidate_solution(self, candidate):
        if len(candidate) != self.num_vertices:
            return False
        for vertex in range(self.num_vertices):
            if candidate[vertex] == -1:
                return False
            vertex_neighbors = self.get_neighbors(vertex)
            for neighbor in vertex_neighbors:
                if candidate[vertex] == candidate[neighbor]:
                    return False
        return True

    def is_independent_set(self, vertex, independent_set):
        for element in independent_set:
            if self.adj_matrix[element][vertex]:
                return False
        return True

    def visualize_graph(self):
        G = nx.Graph()
        G.add_nodes_from([i for i in range(self.num_vertices)])
        G.add_edges_from(self.edges)
        nx.draw_networkx(G, node_color=self.colors)
        plt.show()

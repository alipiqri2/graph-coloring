import time

recursion_depth = 0

def isSafe(g, vertex, c):
    for i in range(g.num_vertices):
        if g.adj_matrix[vertex][i] == 1 and g.colors[i] == c:
            return False
    return True


def graphColourUtil(g, n_color, v):
    global recursion_depth
    recursion_depth += 1
    if v == g.num_vertices:
        return True

    for c in range(1, n_color + 1):
        if isSafe(g, v, c):
            g.colors[v] = c
            if graphColourUtil(g, n_color, v + 1):
                return True
            g.colors[v] = 0


def measured_backtracking(g, n_color):
    g.colors = [0] * g.num_vertices
    print(g.colors, n_color)
    global recursion_depth
    recursion_depth = 0
    print(f"Number of vertices: {g.num_vertices}, number of edges: {g.num_edges}")
    print(g.__dict__)

    start_time = time.time()
    if graphColourUtil(g, n_color, 0) is None:
        g.optimum = 0
        solution = False
    else:
        g.optimum = n_color
        solution = True

    end_time = time.time()

    print("optimum number of colors: ", g.optimum)
    print("coloring: ", g.colors)
    print("recursion depth: ", recursion_depth)
    print("solution: ", solution)
    print("Execution time: ", end_time - start_time)

    return g.optimum, end_time - start_time, solution, recursion_depth

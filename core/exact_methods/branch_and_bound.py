import time

recursion_depth = 0


def branch_and_bound_recursive(g, return_on_first_leaf=False):

    def search(g, sub_coloring, num_colors, num_non_colored):
        global recursion_depth
        recursion_depth += 1

        if return_on_first_leaf and g.optimum < g.num_vertices + 1:
            return

        print(f'{sub_coloring} {num_colors} {num_non_colored} {recursion_depth}')
        if num_non_colored == 0 and num_colors < g.optimum:
            g.optimum = num_colors
            g.colors = sub_coloring.copy()
            return

        # Cut the branch if the number of colors used so far
        # is already greater than or equal to the optimum.
        if num_colors >= g.optimum:
            return

        # Introduce a new color.
        new_color = num_colors
        print(f"new_color = {new_color}")

        # Create a list to contain the children of this node.
        extended_sub_coloring = []

        # Create children for this node.
        for i in range(len(sub_coloring)):
            print(f"\tcreate children = {i} of {len(sub_coloring)}")
            # Find next uncolored vertex.
            if sub_coloring[i] != -1:
                print(f"\tvertex already colored")
                continue

            # Initialize a new child.
            not_yet_colored = num_non_colored
            possibility = sub_coloring.copy()
            print(f"\tinitialize child, not yet colored = {not_yet_colored} of {possibility}")

            # Color the uncolored vertex found with the new color.
            possibility[i] = new_color
            not_yet_colored -= 1
            print(f"\tcolor the uncolored vertex found with the new color, not yet colored = {not_yet_colored} of {possibility}")

            # Color as many vertices as possible with the new color.
            for j in range(len(possibility)):
                print(f"\t\tloop for possibility {j} of {len(possibility)}")
                # Find next uncolored vertex.
                if possibility[j] != -1:
                    print(f"\t\tthe possibility vertex already colored")
                    continue

                # Color the found uncolored vertex with the new color
                # if there is no conflict with the olors of the neighbors.
                neighbors_colors = g.get_neighbors_colors(j, possibility)
                print(f"\t\tneighbors_colors {neighbors_colors}")
                if new_color not in neighbors_colors:
                    possibility[j] = new_color
                    not_yet_colored -= 1
                    print(f"\t\tcolor the uncolored vertex found with the new color, not yet colored = {not_yet_colored} of {possibility}")

            # Add the new child to the list.
            if (not_yet_colored, possibility) not in extended_sub_coloring:
                extended_sub_coloring.append((not_yet_colored, possibility))
                print(f"\tadd child = {not_yet_colored} of {possibility}, the result {extended_sub_coloring}")

        # Sort threads in ascending order
        # depending on the value of not_yet_colored (closest to the sheet).
        extended_sub_coloring.sort()
        print(f"sorting the extended {extended_sub_coloring}")

        # Call `search` on each created child.
        for extended in extended_sub_coloring:
            search(g, extended[1], new_color + 1, extended[0])

    # Initialize root node with all vertices uncolored.
    # Initialize the optimum with a value greater than the worst case.
    sub_coloring = [-1 for _ in range(g.num_vertices)]
    print(sub_coloring)
    g.optimum = g.num_vertices + 1

    # Start a reseach.
    search(g, sub_coloring, 0, g.num_vertices)
    print("recursion_depth is; ", recursion_depth)


def measured_branch_and_bound(g, return_on_first_leaf=False):
    global recursion_depth
    recursion_depth = 0
    print(f"Number of vertices: {g.num_vertices}, number of edges: {g.num_edges}")
    print(g.__dict__)

    start_time = time.time()
    branch_and_bound_recursive(g, return_on_first_leaf)

    end_time = time.time()

    print("optimum number of colors: ", g.optimum)
    print("coloring: ", g.colors)
    print("Execution time: ", end_time - start_time)

    return g.optimum, end_time - start_time, recursion_depth
